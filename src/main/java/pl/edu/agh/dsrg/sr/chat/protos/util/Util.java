package pl.edu.agh.dsrg.sr.chat.protos.util;

import org.jgroups.Header;
import org.jgroups.Message;
import org.jgroups.View;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos;
import pl.edu.agh.dsrg.sr.chat.protos.adapter.Accepter;
import pl.edu.agh.dsrg.sr.chat.protos.adapter.Receiver;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author - Lukasz Gmyrek
 *         Created on  2015-03-28
 */
public class Util {
    public static final byte[] endline = "\n".getBytes();

    public static String stringFromActionType(ChatOperationProtos.ChatAction.ActionType actionType) {
        if (actionType.getNumber() == 0) {
            return "join";
        } else {
            return "leave";
        }
    }

    public static Receiver getIOReceiver(final String prompt, final OutputStream outputStream) {
        return new Receiver() {
            @Override
            public void receive(Message message) {
                try {
                    outputStream.flush();
                    outputStream.write(prompt.getBytes());
                    outputStream.write(("channel: " + channelNameFromHeader(message.getHeader(Short.valueOf("21")))).getBytes());
                    outputStream.write((", sender: " + message.getSrc().toString()).getBytes());
                    outputStream.write("\nmessage: ".getBytes());
                    outputStream.write(message.getBuffer());
                    outputStream.write(endline);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public static Receiver getChatActionReceiver(final String prompt, final OutputStream  outputStream) {
        return new Receiver() {
            @Override
            public void receive(Message message) {
                try {
                    outputStream.flush();
                    outputStream.write(prompt.getBytes());
                    outputStream.write(("channel: " + channelNameFromHeader(message.getHeader(Short.valueOf("21")))).getBytes());
                    outputStream.write((", sender: " + message.getSrc().toString()).getBytes());

                    ChatOperationProtos.ChatAction chatAction = ChatOperationProtos.ChatAction.parseFrom(message.getBuffer());

                    outputStream.write(endline);
                    outputStream.write("action: ".getBytes());
                    outputStream.write(Util.stringFromActionType(chatAction.getAction()).getBytes());
                    outputStream.write(", channel: ".getBytes());
                    outputStream.write(chatAction.getChannel().getBytes());
                    outputStream.write(", nickname: ".getBytes());
                    outputStream.write(chatAction.getNickname().getBytes());
                    outputStream.write(endline);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public static String channelNameFromHeader(Header header) {
        String str = header.toString();
        return str.substring(0, str.length() - 1).split("=")[1];
    }

    public static Accepter getIOAccepter(final String prompt, final OutputStream outputStream) {
        return new Accepter() {
            @Override
            public void accept(View view) {
                try {
                    outputStream.flush();
                    outputStream.write(prompt.getBytes());
                    outputStream.write(view.toString().getBytes());
                    outputStream.write(endline);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }
}
