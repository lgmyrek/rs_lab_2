package pl.edu.agh.dsrg.sr.chat.protos.adapter;

import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;

/**
 * @author - Lukasz Gmyrek
 *         Created on  2015-03-29
 */
public class DefaultReceiverAdapter extends ReceiverAdapter {
    private final Receiver receiver;
    private final Accepter accepter;

    public DefaultReceiverAdapter(Receiver receiver, Accepter accepter) {
        this.receiver = receiver;
        this.accepter = accepter;
    }

    @Override
    public void viewAccepted(View view) {
        accepter.accept(view);
    }

    @Override
    public void receive(Message msg) {
        receiver.receive(msg);
    }
}
