package pl.edu.agh.dsrg.sr.chat.protos;

import com.google.common.base.Optional;
import org.jgroups.*;
import org.jgroups.protocols.*;
import org.jgroups.protocols.pbcast.*;
import org.jgroups.stack.ProtocolStack;
import pl.edu.agh.dsrg.sr.chat.protos.adapter.ManagementReceiverAdapter;

import java.net.InetAddress;
import java.util.*;

/**
 * @author - Lukasz Gmyrek
 *         Created on  2015-03-28
 */
public class JGroupsWrapper {
    public static final int GET_STATE_TIMEOUT = 5000;
    public static final int DEFAULT_PORT = 7600;
    public static final String DEFAULT_MULTICAST_ADDRESS = "228.8.8.8";
    public static final String CHAT_MANAGEMENT_NAME = "ChatManagement768624";

    private JChannel chatManagementChannel;

    private final Map<String, JChannel> connectedChannels;

    private final Collection<ChatOperationProtos.ChatAction> sharedManagementState;

    private final String nickname;

    public JGroupsWrapper(String nickname, ManagementReceiverAdapter adapter) {
        connectedChannels = new HashMap<>();
        this.nickname = nickname;
        this.sharedManagementState = adapter.getSharedState();
        try {
            chatManagementChannel = setupChatManagement(adapter);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error while establishing connection to Chat Cluster shutting down");
            System.exit(-1);
        }
    }

    public void send(String channelName, String msg) throws Exception {
        JChannel jchannel = connectedChannels.get(channelName);
        if (jchannel != null) {
            byte[] serializedMessage =
                    ChatOperationProtos.ChatMessage
                    .newBuilder()
                    .setMessage(msg)
                    .build()
                    .toByteArray();

            Message message = new Message(null, null, serializedMessage);

            jchannel.send(message);
        } else {
            System.out.println("Not connected to chanel: " + channelName);
        }
    }


    private JChannel setupChatChannel(String channelAddress ,ReceiverAdapter adapter) throws Exception {
        JChannel channel = getNewConfiguredChannel(channelAddress);
        channel.setName(nickname);
        channel.setReceiver(adapter);
        channel.connect(channelAddress);
        channel.getState(null, GET_STATE_TIMEOUT);
        return channel;
    }

    private JChannel setupChatManagement(ReceiverAdapter adapter) throws Exception {
        JChannel channel = getNewConfiguredChannel(DEFAULT_MULTICAST_ADDRESS);
        channel.setName(nickname);
        channel.setReceiver(adapter);
        channel.connect(CHAT_MANAGEMENT_NAME);
        channel.getState(null, GET_STATE_TIMEOUT);
        return channel;
    }

    //Naive but what's the point of different
    public Collection<String> getChannels() {
        Collection<String> result = new LinkedList<>();
        Map<String, Integer> channelToConnectedMap = new HashMap<>();

        for (ChatOperationProtos.ChatAction chatAction : sharedManagementState) {
            int action;

            if(chatAction.getAction().equals(ChatOperationProtos.ChatAction.ActionType.JOIN)) {
                action = 1;
            } else {
                action = -1;
            }

            String channel = chatAction.getChannel();

            if (channelToConnectedMap.get(channel) == null) channelToConnectedMap.put(channel, 0);
            channelToConnectedMap.put(channel, channelToConnectedMap.get(channel) + action);
        }

        for (Map.Entry<String, Integer> channelToConnected : channelToConnectedMap.entrySet()) {
            if (channelToConnected.getValue() > 0) result.add(channelToConnected.getKey());
        }

        return result;
    }

    public Optional<List<Address>> getUsersConnectedToChannel(String channelName) {
        JChannel channel = connectedChannels.get(channelName);
        if (channel != null) {
            return Optional.of(channel.getView().getMembers());
        } else {
            return Optional.absent();
        }
    }

    public void joinChannel(String channelName, ReceiverAdapter adapter) throws Exception {
        JChannel channel = connectedChannels.get(channelName);
        if (channel == null) {
            channel = setupChatChannel(channelName, adapter);
            connectedChannels.put(channelName, channel);

            byte[] serializedJoinMessage =
                    ChatOperationProtos.ChatAction
                            .newBuilder()
                            .setAction(ChatOperationProtos.ChatAction.ActionType.JOIN)
                            .setChannel(channelName)
                            .setNickname(nickname)
                            .build()
                            .toByteArray();

            Message joinMessage = new Message(null, null, serializedJoinMessage);

            chatManagementChannel.send(joinMessage);
        } else {
            System.out.println("Already connected");
        }
    }

    public void leaveChannel(String channelName) throws Exception {
        JChannel channel = connectedChannels.get(channelName);

        if (channel != null) {
            channel.disconnect();
            channel.close();

            connectedChannels.remove(channelName);

            byte[] serializedLeaveMessage =
                    ChatOperationProtos.ChatAction
                            .newBuilder()
                            .setAction(ChatOperationProtos.ChatAction.ActionType.LEAVE)
                            .setChannel(channelName)
                            .setNickname(nickname)
                            .build()
                            .toByteArray();

            Message leaveMessage = new Message(null, null, serializedLeaveMessage);

            chatManagementChannel.send(leaveMessage);
        }
    }

    public void closeAll() throws Exception {
        for (JChannel channel : connectedChannels.values()) {
            leaveChannel(channel.getName());
            channel.disconnect();
            channel.close();
        }

        leaveChannel(CHAT_MANAGEMENT_NAME);
        chatManagementChannel.disconnect();
        chatManagementChannel.close();
    }

    private JChannel getNewConfiguredChannel(String address) throws Exception {
        JChannel channel = new JChannel(false);
        ProtocolStack stack = new ProtocolStack();
        channel.setProtocolStack(stack);

        stack.addProtocol(new UDP().setValue("mcast_group_addr", InetAddress.getByName(address)))
                .addProtocol(new PING())
                .addProtocol(new MERGE2())
                .addProtocol(new FD_SOCK())
                .addProtocol(new FD_ALL().setValue("timeout", 12000).setValue("interval", 3000))
                .addProtocol(new VERIFY_SUSPECT())
                .addProtocol(new BARRIER())
                .addProtocol(new NAKACK())
                .addProtocol(new UNICAST2())
                .addProtocol(new STABLE())
                .addProtocol(new GMS())
                .addProtocol(new UFC())
                .addProtocol(new MFC())
                .addProtocol(new FRAG2())
                .addProtocol(new STATE_TRANSFER())
                .addProtocol(new FLUSH());
        stack.init();

        return channel;
    }
}
