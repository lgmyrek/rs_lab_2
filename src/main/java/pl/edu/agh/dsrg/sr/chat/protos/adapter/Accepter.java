package pl.edu.agh.dsrg.sr.chat.protos.adapter;

import org.jgroups.Message;
import org.jgroups.View;

/**
 * @author - Lukasz Gmyrek
 *         Created on  2015-03-28
 */
public interface Accepter {
    void accept(View view);
}
