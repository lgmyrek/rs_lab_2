package pl.edu.agh.dsrg.sr.chat.protos.adapter;

import com.google.protobuf.InvalidProtocolBufferException;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * @author - Lukasz Gmyrek
 *         Created on  2015-03-29
 */
public class ManagementReceiverAdapter extends ReceiverAdapter {
    private final ConcurrentLinkedDeque<ChatOperationProtos.ChatAction> state;

    private final Receiver receiver;
    private final Accepter accepter;

    public ManagementReceiverAdapter(Receiver receiver, Accepter accepter) {
        this.state = new ConcurrentLinkedDeque<>();
        this.receiver = receiver;
        this.accepter = accepter;
    }

    @Override
    public void viewAccepted(View view) {
        accepter.accept(view);
    }

    @Override
    public void setState(InputStream input) throws Exception {
        ChatOperationProtos.ChatState newState = ChatOperationProtos.ChatState.parseFrom(input);

        state.clear();
        state.addAll(newState.getStateList());
    }

    @Override
    public void getState(OutputStream output) throws Exception {
        ChatOperationProtos.ChatState.Builder stateBuilder = ChatOperationProtos.ChatState.newBuilder();
        for (ChatOperationProtos.ChatAction chatAction : state) {
            if(chatAction.getAction().equals(ChatOperationProtos.ChatAction.ActionType.JOIN)) {
                stateBuilder.addState(chatAction);
            }
        }

        stateBuilder.build().writeTo(output);
    }

    @Override
    public void receive(Message msg) {
        try {
            ChatOperationProtos.ChatAction chatAction = ChatOperationProtos.ChatAction.parseFrom(msg.getBuffer());
            state.add(chatAction);
            receiver.receive(msg);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }


    public Collection<ChatOperationProtos.ChatAction> getSharedState() {
        return Collections.unmodifiableCollection(state);
    }
}
