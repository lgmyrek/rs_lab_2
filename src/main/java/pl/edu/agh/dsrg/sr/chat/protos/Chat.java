package pl.edu.agh.dsrg.sr.chat.protos;

import java.util.Collection;

/**
 * Created by lgmyrek on 3/28/15.
 */
public interface Chat {
    void start();
    void send(String channelName, String message);
    void newChannel(String name);
    void joinChannel(String name);
    Collection<String> getChannels();
    Collection<String> getChannelUsers(String channel);
    void close();
}
