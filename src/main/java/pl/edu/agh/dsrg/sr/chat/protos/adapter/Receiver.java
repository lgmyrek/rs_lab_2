package pl.edu.agh.dsrg.sr.chat.protos.adapter;

import org.jgroups.Message;

/**
 * @author - Lukasz Gmyrek
 *         Created on  2015-03-28
 */
public interface Receiver {
    void receive(Message message);
}
