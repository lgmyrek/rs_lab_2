package pl.edu.agh.dsrg.sr.chat.protos;

import pl.edu.agh.dsrg.sr.chat.protos.adapter.DefaultReceiverAdapter;
import pl.edu.agh.dsrg.sr.chat.protos.adapter.ManagementReceiverAdapter;
import pl.edu.agh.dsrg.sr.chat.protos.util.IOComposition;
import scala.sys.ShutdownHookThread;

import static pl.edu.agh.dsrg.sr.chat.protos.util.Util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author - Lukasz Gmyrek
 *         Created on  2015-03-28
 */
public class Main {
    public static void main(String[] args) {
        String nickname;
        if (args.length < 1) {
            nickname = "DefaultNickname_" + ThreadLocalRandom.current().nextLong(187236816237816L);
        } else {
            nickname = args[0];
        }
        IOComposition io = new IOComposition(System.in, System.out);

        DefaultReceiverAdapter dAdapter = new DefaultReceiverAdapter(
                getIOReceiver("received from: ", io.getOutput()),
                getIOAccepter("accepted from: ", io.getOutput()));

        ManagementReceiverAdapter adapter = new ManagementReceiverAdapter(
                getChatActionReceiver("received from: ", io.getOutput()),
                getIOAccepter("accepted from: ", io.getOutput())
        );

        JGroupsWrapper jGroupsWrapper =
                new JGroupsWrapper(nickname, adapter);


        final Chat chat = new DefaultChat(nickname, io, jGroupsWrapper, dAdapter);

        chat.start();
    }
}
