package pl.edu.agh.dsrg.sr.chat.protos.util;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author - Lukasz Gmyrek
 *         Created on  2015-03-28
 */
public class IOComposition {
    private final InputStream input;
    private final OutputStream output;

    public IOComposition(InputStream input, OutputStream output) {
        this.input = input;
        this.output = output;
    }

    public InputStream getInput() {
        return input;
    }

    public OutputStream getOutput() {
        return output;
    }
}
