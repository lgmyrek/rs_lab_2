package pl.edu.agh.dsrg.sr.chat.protos;

import com.google.common.base.Optional;
import org.jgroups.Address;
import org.jgroups.ReceiverAdapter;
import pl.edu.agh.dsrg.sr.chat.protos.util.IOComposition;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by lgmyrek on 3/28/15.
 */
public class DefaultChat implements Chat {
    private final String nickname;

    public static final int ENDLINE = 10;
    private final String defaultPrompt;

    public static final String END_SEQUENCE = "exit";
    public static final String USERS_SEQUENCE = "users";
    public static final String NEW_SEQUENCE = "new";
    public static final String JOIN_SEQUENCE = "join";
    public static final String CHANNELS_SEQUENCE = "channels";
    public static final String LEAVE_SEQUENCE ="leave";

    private final OutputStream output;
    private final InputStream input;

    private final JGroupsWrapper jGroupsWrapper;
    private final ReceiverAdapter adapter;


    public DefaultChat(String nickname, IOComposition io, JGroupsWrapper jGroupsWrapper, ReceiverAdapter ra) {
        this.output = io.getOutput();
        this.input = io.getInput();
        this.nickname = nickname;
        defaultPrompt = "> ";
        this.jGroupsWrapper = jGroupsWrapper;
        this.adapter = ra;
    }

    @Override
    public void start() {
        while(true) {
            try {
                output.write(defaultPrompt.getBytes());
                output.flush();

                StringBuilder stringBuilder = new StringBuilder();
                int ch;
                do {
                    ch = input.read();
                    if(ch > 27) stringBuilder.append((char)ch);
                } while (ch != ENDLINE);
                String input = stringBuilder.toString();
                if (input.toLowerCase().equals(END_SEQUENCE)) {
                    close();
                    System.exit(0);
                }
                else if (input.toLowerCase().startsWith(NEW_SEQUENCE) || input.toLowerCase().startsWith(JOIN_SEQUENCE)) {
                    newChannel(input.split(" ")[1]);
                } else if (input.toLowerCase().startsWith(USERS_SEQUENCE)) {
                    String address = input.split(" ")[1];
                    Collection<String> users = getChannelUsers(address);

                    output.write(("users of channel: " + address + " : ").getBytes());
                    for (String user : users) {
                        output.write((user + ", ").getBytes());
                    }
                    output.write("\n".getBytes());
                } else if (input.toLowerCase().startsWith(CHANNELS_SEQUENCE)) {
                    Collection<String> channels = getChannels();

                    output.write(("channels: ").getBytes());
                    for (String channel : channels) {
                        output.write((channel + ", ").getBytes());
                    }
                    output.write("\n".getBytes());
                }else if (input.toLowerCase().startsWith(LEAVE_SEQUENCE)){
                    String address = input.split(" ")[1];
                    try {
                        jGroupsWrapper.leaveChannel(address);
                        output.write(("leaving: " + address + " channel. \n").getBytes());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    String address = input.split(" ")[0];
                    send(address, input.substring(address.length() + 1));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void send(String channelName, String message) {
        try {
            jGroupsWrapper.send(channelName, message);
        } catch (Exception e) {
            try {
                e.printStackTrace();
                output.flush();
                output.write("Error while sending message. \n".getBytes());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void newChannel(String name) {
        joinChannel(name);
    }

    @Override
    public void joinChannel(String name) {
        try {
            jGroupsWrapper.joinChannel(name, adapter);
        } catch (Exception e) {
            try {
                output.flush();
                output.write("Error while joining channel. \n".getBytes());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public Collection<String> getChannels() {
        return jGroupsWrapper.getChannels();
    }

    @Override
    public Collection<String> getChannelUsers(String channel) {
        List<String> result = new LinkedList<>();
        Optional<List<Address>> opt = jGroupsWrapper.getUsersConnectedToChannel(channel);
        if (opt.isPresent()) {
            for (Address address : opt.get()) {
                result.add(address.toString());
            }
        }
        return result;
    }

    @Override
    public void close() {
        try {
            jGroupsWrapper.closeAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
